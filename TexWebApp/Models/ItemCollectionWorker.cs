﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Text;
using Newtonsoft.Json;

namespace TexWebApp.Models
{
    public static class ItemCollectionWorker
    {
        private static string StoragePath = string.Concat(Directory.GetCurrentDirectory(), "\\", Startup.Configuration["Storage:FilePath"]);
        private static object locker = new object();

        private static List<Item> items_list;
        public static List<Item> ItemsList {
            get
            {
                lock(locker)
                {
                    return items_list;
                }
            }
            set
            {
                lock (locker)
                {
                    items_list = value;
                }
            }
        }

        static ItemCollectionWorker()
        {
            LoadFromJsonFile();
        }

        public static string GetAllItemsJson()
        {
            try
            {
                return JsonConvert.SerializeObject(ItemsList);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return "[]";
            }
        }

        public static string GetItemJson(int id)
        {
            try
            {
                if (ItemsList.Exists(x => x.Id == id))
                {
                    return JsonConvert.SerializeObject(ItemsList.Find(x => x.Id == id));
                }
                return "[]";
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return "[]";
            }
        }

        public static int AddItem(Stream stream)
        {
            try
            {
                string json_response = GetJsonFromStream(stream, Encoding.UTF8);
                Item new_item = JsonConvert.DeserializeObject<Item>(json_response);
                int new_id = TakeId();
                if (new_id != 0)
                {
                    new_item.Id = new_id;
                    ItemsList.Add(new_item);
                    SaveToJsonFile();
                    return 200;
                }
                return 500;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return 500;
            }
        }

        public static int ChangeItem(int id, Stream stream)
        {
            try
            {
                string json_response = GetJsonFromStream(stream, Encoding.UTF8);
                Item new_item = JsonConvert.DeserializeObject<Item>(json_response);

                if (ItemsList.Exists(x => x.Id == id))
                {
                    Item item = ItemsList.Find(x => x.Id == id);
                    item.Name = new_item.Name;
                    item.Image = new_item.Image;
                    SaveToJsonFile();
                    return 200;
                }
                return 500;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return 500;
            }
        }

        public static int DeleteItem(int id)
        {
            if (ItemsList.Exists(x => x.Id == id))
            {
                ItemsList.RemoveAll(x => x.Id == id);
                SaveToJsonFile();
                return 200;
            }
            return 500;
        }

        public static string GetJsonFromStream(Stream stream, Encoding enc)
        {
            byte[] bytes;
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                bytes = ms.ToArray();
            }
            return enc.GetString(bytes);
        }

        public static void SaveToJsonFile()
        {
            try
            {
                DataContractJsonSerializer json_formatter = new DataContractJsonSerializer(typeof(List<Item>));

                using (FileStream stream = new FileStream(StoragePath, FileMode.Create, FileAccess.Write, FileShare.ReadWrite))
                {
                    json_formatter.WriteObject(stream, ItemsList);
                    stream.Close();
                }
            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void LoadFromJsonFile()
        {
            try
            {
                DataContractJsonSerializer json_formatter = new DataContractJsonSerializer(typeof(List<Item>));

                using (FileStream stream = new FileStream(StoragePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    ItemsList = (List<Item>)json_formatter.ReadObject(stream);
                }

            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static int TakeId()
        {
            if (ItemsList.Count != 0)
            {
                int i = ItemsList.Last().Id + 1;
                int start = i;
                while (ItemsList.Exists(x => x.Id == i))
                {
                    if (i == int.MaxValue)
                    {
                        i = 1;
                    }
                    if (i == start)
                    {
                        return 0;
                    }
                    i++;
                }
                return i;
            }
            else
            {
                return 1;
            }
        }
    }
}
