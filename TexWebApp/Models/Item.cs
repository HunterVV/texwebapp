﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Drawing;
using System.Runtime.Serialization;
using System.IO;
using System.ComponentModel;
using System.Text;

namespace TexWebApp.Models
{
    [DataContract]
    public class Item
    {
        [DataMember(Name = "Id")]
        public int Id { get; set; }

        [DataMember(Name = "Name")]
        public string Name { get; set; }
        
        [IgnoreDataMember]
        public Bitmap Image { get; set; }

        [DataMember(Name = "ImageByteArray")]
        public byte[] ImageByteArray
        {
            get
            {
                if (Image != null)
                {

                    using (var stream = new MemoryStream())
                    {
                        Image.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                        stream.Close();
                        return stream.ToArray();
                    }
                }
                else
                {
                    return null;
                }
            }

            set
            {
                if (value != null)
                {
                    Image = new Bitmap(new MemoryStream(value));
                }
                else
                { 
                    Image = null;
                }
            }
        }

        /*[DataMember(Name = "ImageByteArray")]
        public string ImageByteArray
        {
            get
            {
                if (Image != null)
                {

                    using (var stream = new MemoryStream())
                    {
                        Image.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                        stream.Close();
                        return Encoding.ASCII.GetString(stream.ToArray());
                    }
                }
                else
                {
                    return null;
                }
            }

            set
            {
                if (value != null)
                {
                    Image = new Bitmap(new MemoryStream(Encoding.ASCII.GetBytes(value)));
                }
                else
                {
                    Image = null;
                }
            }
        }*/
    }
}
