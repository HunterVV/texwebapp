﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;
using Newtonsoft.Json;
using TexWebApp.Models;

namespace TexWebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ItemsController : ControllerBase
    {
        public ItemsController()
        {
            
        }

        [HttpGet]
        public ActionResult Get()
        {
            string json_result = ItemCollectionWorker.GetAllItemsJson();
            if (json_result == "[]")
            {
                Response.StatusCode = 204;
            }
            return Content(json_result);
        }

        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            string json_result = ItemCollectionWorker.GetItemJson(id);
            if (json_result == "[]")
            {
                Response.StatusCode = 204;
            }
            return Content(json_result);
        }

        [HttpPost]
        public void Post()
        {
            using (Stream stream = Request.Body)
            {
                Response.StatusCode = ItemCollectionWorker.AddItem(stream);
            }
        }

        [HttpPut("{id}")]
        public void Put(int id)
        {
            using (Stream stream = Request.Body)
            {
                Response.StatusCode = ItemCollectionWorker.ChangeItem(id, stream);
            }
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            Response.StatusCode = ItemCollectionWorker.DeleteItem(id);
        }
    }
}
